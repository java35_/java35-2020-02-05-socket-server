package v3;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import v3.dto.Car;

public class Server implements Closeable {
	int port;
	ServerSocket socket;
	
	public Server(int port) throws Exception {
		this.port = port;
		socket = new ServerSocket(port);
	}
	
	public void run() throws Exception {
		System.out.println("Server started...");
		while (true) {
			try {
				Socket serverSocket = socket.accept();
				System.out.println("-----------------------------------------------------");
				System.out.println("Server listening on address: " + serverSocket.getLocalAddress());
				System.out.println("Server listening on port: " + serverSocket.getLocalPort());
				System.out.println("Server listening on: " + serverSocket.getLocalSocketAddress());
				
				System.out.println("Client on address: " + serverSocket.getInetAddress());
				System.out.println("Client on port: " + serverSocket.getPort());
				System.out.println("Client on: " + serverSocket.getRemoteSocketAddress());
				System.out.println("-----------------------------------------------------");
				
				InputStream is = serverSocket.getInputStream();
				OutputStream os = serverSocket.getOutputStream();
				
				ObjectOutputStream oos = new ObjectOutputStream(os);
				ObjectInputStream ois = new ObjectInputStream(is);
				
				Car incomingCar = (Car)ois.readObject();
				System.out.println("Recieved: " + incomingCar);
				
				oos.writeObject(incomingCar);
				System.out.println("Sent: " + incomingCar);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void close() throws IOException {
		socket.close();
		
	}
}
