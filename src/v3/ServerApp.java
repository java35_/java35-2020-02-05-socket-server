package v3;

public class ServerApp {

	public static void main(String[] args) throws Exception {
		Server server = new Server(5000);
		server.run();
		server.close();
	}

}
