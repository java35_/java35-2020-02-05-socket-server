package v2;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import v2.dto.Car;

public class ServerApp {

	public static void main(String[] args) throws Exception {
		System.out.println("Server started...");
		ServerSocket socket = new ServerSocket(5000);
		
		while (true) {
			try {
				Socket serverSocket = socket.accept();
				System.out.println("-----------------------------------------------------");
				System.out.println("Server listening on address: " + serverSocket.getLocalAddress());
				System.out.println("Server listening on port: " + serverSocket.getLocalPort());
				System.out.println("Server listening on: " + serverSocket.getLocalSocketAddress());
				
				System.out.println("Client on address: " + serverSocket.getInetAddress());
				System.out.println("Client on port: " + serverSocket.getPort());
				System.out.println("Client on: " + serverSocket.getRemoteSocketAddress());
				System.out.println("-----------------------------------------------------");
				
				InputStream is = serverSocket.getInputStream();
				OutputStream os = serverSocket.getOutputStream();
				
				ObjectOutputStream oos = new ObjectOutputStream(os);
				ObjectInputStream ois = new ObjectInputStream(is);
				
				Car incomingCar = (Car)ois.readObject();
				System.out.println("Recieved: " + incomingCar);
				
				oos.writeObject(incomingCar);
				oos.flush();
				System.out.println("Sent: " + incomingCar);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
