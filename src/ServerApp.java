import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerApp {

	public static void main(String[] args) throws Exception {
		System.out.println("Server started...");
		ServerSocket socket = new ServerSocket(5000);
		
		Socket serverSocket = socket.accept();
		System.out.println("-----------------------------------------------------");
		System.out.println("Server listening on address: " + serverSocket.getLocalAddress());
		System.out.println("Server listening on port: " + serverSocket.getLocalPort());
		System.out.println("Server listening on: " + serverSocket.getLocalSocketAddress());
		
		System.out.println("Client on address: " + serverSocket.getInetAddress());
		System.out.println("Client on port: " + serverSocket.getPort());
		System.out.println("Client on: " + serverSocket.getRemoteSocketAddress());
		System.out.println("-----------------------------------------------------");
		
		InputStream is = serverSocket.getInputStream();
		OutputStream os = serverSocket.getOutputStream();
		
		ObjectOutputStream oos = new ObjectOutputStream(os);
		ObjectInputStream ois = new ObjectInputStream(is);
		
		String incomingMessage = (String)ois.readObject();
		System.out.println("Recieved: " + incomingMessage);
		
		oos.writeObject(incomingMessage);
		System.out.println("Sent: " + incomingMessage);
		
		System.out.println("Server off");
	}

}
